var countryList = ['BD', 'BE', 'BF', 'BG', 'BA', 'BN', 'BO', 'JP', 'BI', 'BJ', 'BT', 'JM', 'BW', 'BR', 'BS', 'BY', 'BZ', 'RU', 'RW', 'RS', 'TL', 'TM', 'TJ', 'RO', 'GW', 'GT', 'GR', 'GQ', 'GY', 'GE', 'GB', 'GA', 'GN', 'GM', 'GL', 'GH', 'OM', 'TN', 'JO', 'HR', 'HT', 'HU', 'HN', 'PR', 'PS', 'PT', 'PY', 'PA', 'PG', 'PE', 'PK', 'PH', 'PL', 'ZM', 'EH', 'EE', 'EG', 'ZA', 'EC', 'IT', 'VN', 'SB', 'ET', 'SO', 'ZW', 'ES', 'ER', 'ME', 'MD', 'MG', 'MA', 'UZ', 'MM', 'ML', 'MN', 'MK', 'MW', 'MR', 'UG', 'MY', 'MX', 'IL', 'FR', 'XS', 'FI', 'FJ', 'FK', 'NI', 'NL', 'NO', 'NA', 'VU', 'NC', 'NE', 'NG', 'NZ', 'NP', 'XK', 'CI', 'CH', 'CO', 'CN', 'CM', 'CL', 'XC', 'CA', 'CG', 'CF', 'CD', 'CZ', 'CY', 'CR', 'CU', 'SZ', 'SY', 'KG', 'KE', 'SS', 'SR', 'KH', 'SV', 'SK', 'KR', 'SI', 'KP', 'KW', 'SN', 'SL', 'KZ', 'SA', 'SE', 'SD', 'DO', 'DJ', 'DK', 'DE', 'YE', 'DZ', 'US', 'UY', 'LB', 'LA', 'TW', 'TT', 'TR', 'LK', 'LV', 'LT', 'LU', 'LR', 'LS', 'TH', 'TF', 'TG', 'TD', 'LY', 'AE', 'VE', 'AF', 'IQ', 'IS', 'IR', 'AM', 'AL', 'AO', 'AR', 'AU', 'AT', 'IN', 'TZ', 'AZ', 'IE', 'ID', 'UA', 'QA', 'MZ']

var timeStep = 15

function numberFormat (number, decimals, decPoint, thousandsSep) {
  // *     example: numberFormat(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '')
  var n = !isFinite(+number) ? 0 : +number

  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)

  var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep

  var dec = (typeof decPoint === 'undefined') ? '.' : decPoint

  var s = ''

  var toFixedFix = function (n, prec) {
    var k = Math.pow(10, prec)
    return '' + Math.round(n * k) / k
  }
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}

function createPortChart (aa) {
  var all = Object.assign({}, aa.alerts, aa.normal)
  var labelsList = []
  var dataAlert = []
  var dataNormal = []

  function get (object, key, defaultValue) {
    var result = defaultValue
    if (key in object) {
      result = object[key]
    }
    return result
  }

  for (var port in all) {
    labelsList.push(port)
    dataAlert.push(get(aa.alerts, port, 0))
    dataNormal.push(get(aa.normal, port, 0))
  }

  if (aa.length == 0) {
    console.warn('createPortChart', 'No data')
    return 0
  }

  for (var i = 0; i < dataAlert.length; i++) {
    if ((dataAlert[i] == 0) && (dataNormal[i] == 0)) {
      dataAlert.splice(i, 1)
      dataNormal.splice(i, 1)
      labelsList.splice(i, 1)
    }
  }

  var allData = []
  for (var i = 0; i < dataAlert.length; i++) {
    if ((dataNormal[i] != 0) && (dataAlert[i] != 0)) {
      allData.push({
        'normal': dataNormal[i],
        'alert': dataAlert[i],
        'port': labelsList[i]
      })
    }
  }

  /*
  //clean data
    dataNormal=Array.apply(null, Array(allData.length)).map(Number.prototype.valueOf,0);
    dataAlert=Array.apply(null, Array(allData.length)).map(Number.prototype.valueOf,0);
  //dataNormal=[]
  //dataAlert=[]

  //Alert data
  for (var i=0; i<allData.length; i++) {
    port = allData[i].port
    val = allData[i].alert
    index = labelsList.indexOf(port)
    dataAlert[index]=val
    //console.log(index,port)
  }

  //sort alert
  allData.sort(function(a, b) {
    return ((b.alert < a.alert) ? -1 : ((b.alert == a.alert) ? 0 : 1));
  })

  for (var i=0; i<allData.slice(0,50).length; i++) {
    port = allData[i].port
    val = allData[i].normal
    index = labelsList.indexOf(port)
    dataNormal[index]=val
  }

  //sort normal
  allData.sort(function(a, b) {
    return ((b.normal < a.normal) ? -1 : ((b.normal == a.normal) ? 0 : 1));
  });

  //remove empty

  for (var i=0; i<allData.length; i++) {
    //console.log(dataAlert[i],dataNormal[i])
    if ((dataAlert[i]==0) && (dataNormal[i]==0)) {
        dataAlert.splice(i,1)
        dataNormal.splice(i,1)
        labelsList.splice(i,1)
    }
  }
  */

  // console.log(allData)

  // Bar Chart
  var ctx = document.getElementById('portChart')
  var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: labelsList,
      datasets: [{
        label: 'Alerts data',
        backgroundColor: '#98383E',
        hoverBackgroundColor: '#2e59d9',
        borderColor: '#4e73df',
        data: dataAlert
      }, {
        label: 'Normal data',
        backgroundColor: '#4e73df',
        hoverBackgroundColor: '#2e59d9',
        borderColor: '#4e73df',
        data: dataNormal
      }
      ]
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      animation: {
        duration: 0
      },
      scales: {
        xAxes: [{
          stacked: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          maxBarThickness: 25,
          scaleLabel: {
            display: true,
            labelString: 'Port number'
          }
        }],
        yAxes: [{
          type: 'logarithmic',
          stacked: true,
          ticks: {
            min: 0,
            maxTicksLimit: 5,
            padding: 10,
            callback: function (value, index, values) {
              return value
              // return numberFormat(value / 1000.0) + ' kB'
              /*
                if ((value>1000) && (value<1000000) ){
                    return numberFormat(value/1000) + ' kB'
                }else if(value >1000000){
                    return numberFormat(value/1000000) + ' MB'
                }else{
                    return numberFormat(value) + ' B'
                } */
            }
          },
          gridLines: {
            color: 'rgb(234, 236, 244)',
            zeroLineColor: 'rgb(234, 236, 244)',
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          },
          scaleLabel: {
            display: true,
            labelString: 'Recieved bytes'
          }
        }]
      },
      legend: {
        display: true,
        position: 'bottom'
      },
      tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: 'rgb(255,255,255)',
        bodyFontColor: '#858796',
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
          label: function (tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || ''
            var dataSize = tooltipItem.yLabel
            if (dataSize > 1000 && dataSize < 1000000) {
              return datasetLabel + ': ' + numberFormat(tooltipItem.yLabel / 1000) + 'kB'
            } else if (dataSize > 1000000) {
              return datasetLabel + ': ' + numberFormat(tooltipItem.yLabel / 1000000) + 'MB'
            } else {
              return datasetLabel + ': ' + numberFormat(tooltipItem.yLabel) + 'B'
            }
          }
        }
      }
    }
  })
}

function setBoxAlerts (data) {
  var alertsCount = data.length
  $('#stats_all_alerts').html(alertsCount.toString())
}

function setBoxAttackers (data) {
  var uniqueAlertsDict = {}
  data.forEach(function (item) {
    if (item['src_ip'] in uniqueAlertsDict) {
      uniqueAlertsDict[item['src_ip']] += 1
    } else {
      uniqueAlertsDict[item['src_ip']] = 1
    }
  })
  var uniqueAlerts = Object.keys(uniqueAlertsDict).length
  var allAlerts = data.length
  $('#stats_unique_attackers').html(uniqueAlerts.toString())
}

function setTblAlerts (data) {
  $('#top10_tbl').html('')
  data.reverse().slice(0, 10).forEach(function (item) {
    if(item.country_iso == null) {
       item.country = "Unknown country"
       item.country_iso = ""
    }
    $('#top10_tbl').append(
      '<tr><td>' + '<span class="flag-icon flag-icon-' + item.country_iso.toLowerCase() + '"></span>&nbsp;' +
            item.country + '</td>' +
            '<td>' + item.dt_end + '</td>' +
            '<td>' + item.severity + '</td>' +
            '<td>' + item.src_ip + '</td>' +
            '<td>' + item.category + '</td>' +
            '<td>' + item.signature + '</td></tr>'
    )
  })
}

/* System uptime */
function setBoxUptime (data) {
  $('#system_uptime').html(data['uptime'])
}

function setBoxLudusStatus (data) {
  $('#stats_ludus_status').html(data['status'])
}

function setTblCountryAlerts (data) {
  var tbl = {}
  $('#top10').html('')

  if (data.length == 0) {
    console.warn('setTblCountryAlerts', 'No data')
    return
  }

  // Create dict. with country stats
  data.forEach(function (item) {
    if (item['country_iso'] in tbl) {
      tbl[item['country_iso']]['count'] += 1
      tbl[item['country_iso']]['src_ip'].add(item['src_ip'])
    } else {
      var tblTmp = {}
      tblTmp.country = item['country']
      tblTmp.count = 1
      tblTmp.src_ip = new Set([item['src_ip']])
      tbl[item['country_iso']] = tblTmp
    }
  })

  // Sort country by alerts count
  var tblTmp = []
  for (var key in tbl) {
    var item = tbl[key]
    item['country_iso'] = key
    tblTmp.push(item)
  }
  tblTmp.sort(function (a, b) { return b.count - a.count })

  // Create table from dict.
  var maxI = (tblTmp.length >= 5) ? 5 : tblTmp.length
  for (var i = 0; i < maxI; i++) {
    item = tblTmp[i]
    if (item['country'] == null) {
      item['country'] = 'Unknown country'
    }
    $('#top10').append('<tr><td>' +
                        '<span class="flag-icon flag-icon-' +
                        item['country_iso'].toLowerCase() + '"></span> ' +
                        item['country'] + '</td><td>' +
                        item['count'] + '</td><td>' +
                        item['src_ip'].size + '</td></tr>')
  }
}

function createAlertMap (data) {
  var countryAlerts = {}

  // Create dict
  for (var i = 0, len = countryList.length; i < len; i++) {
    countryAlerts[countryList[i]] = 0
  }

  // Create dict. with country alerts stat
  data.forEach(function (item) {
    if (item['country_iso'] in countryAlerts) {
      countryAlerts[item['country_iso']] += 1
    }
  })

  // Remove old map object
  try { $('#world-map').vectorMap('get', 'mapObject').remove() } catch (err) {}

  // Create new map object
  $('#world-map').vectorMap({
    map: 'world_mill',
    backgroundColor: '',
    series: {
      regions: [{
        values: countryAlerts,
        scale: ['#C8EEFF', '#0071A4'],
        normalizeFunction: 'polynomial',
        legend: {
          horizontal: true,
          cssClass: 'jvectormap-legend-bg',
          title: 'Number of alerts',
          labelRender: function (v) {
            return v
          }
        }

      }]
    },

    onRegionTipShow: function (e, el, code) {
      el.html('Number of alerts from ' + el.html() + ' - ' + countryAlerts[code])
    }
  })
}

function roundTime (strDate, stepMin = 15) {
  var time = strDate.split(' ')[1]
  var hours = parseInt(time.split(':')[0])
  var min = parseInt(time.split(':')[1])
  var sec = parseInt(time.split(':')[2])
  var strMin = ''
  var strHour = ''
  var strAll = ''
  // 6:0', '6:15', '6:30', '6:45'

  var prevI = 0
  var ret = null
  for (var i = 1; i < Math.round(60 / stepMin); i++) {
    var a = (prevI * stepMin)
    var b = (stepMin * i)

    if ((a <= min) && (min < b)) {
      ret = prevI * stepMin
    }

    prevI = i
  }
  if (ret == null) {
    ret = prevI * stepMin
  }

  strMin = ret.toString()
  strHour = hours.toString()
  if (strMin.length == 1) {
    strMin = '0' + strMin
  }
  if (strHour.length == 1) {
    strHour = '0' + strHour
  }

  strAll = strHour + ':' + strMin
  return strAll
}

function createAlertTimelineChart (aa) {
  function diffHours (dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000
    diff /= (60 * 60)
    return Math.abs(Math.round(diff))
  }

  function formatDate (d) {
    var datestring = ('0' + d.getDate()).slice(-2) + '-' + ('0' + (d.getMonth() + 1)).slice(-2) +
         ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2)
    return datestring
  }

  function getDateArray (start, end, minStep) {
    var arr = new Array()

    var dt = new Date(start)

    while (dt <= end) {
      arr.push(formatDate(new Date(dt)))
      dt.setUTCMinutes(dt.getUTCMinutes() + minStep)
    }
    return arr
  }

  var colorList = ['#FFD300', '#FF8C00', '#FF4600', '#FF0000']
  var colorList = ['#FFD300', '#fa8900', '#FF0000', '#a80000']
  var severityLevels = 4
  var timeStep = 15
  var dates = []

  if (aa.length > 0) {
    for (var i = 0; i < aa.length; i++) {
      var item = aa[i]
      var rtime = roundTime(item['dt_end'])
      var date = item['dt_end'].split(' ')[0]
      var strDate = date + 'T' + rtime + ':00'
      var tmpdate = new Date($.trim(strDate))
      dates.push(new Date(strDate))
    }

    var minDate = dates.reduce(function (a, b) { return a.getTime() < b.getTime() ? a : b })
    var maxDate = dates.reduce(function (a, b) { return a > b.getTime() ? a.getTime() : b })
    var numberItems = diffHours(maxDate, minDate) * Math.round(60 / timeStep)

    var timeList = getDateArray(minDate, maxDate, timeStep)

    // Init alertsList
    var alertsList = new Array(severityLevels)
    for (var i = 0; i < alertsList.length; i++) {
      alertsList[i] = new Array(timeList.length)
    }

    // Zero all data
    for (var i = 0; i < severityLevels; i++) {
      for (var y = 0; y < alertsList[i].length; y++) {
        alertsList[i][y] = 0
      }
    }

    // Set data for severity levels
    for (var i = 0; i < aa.length; i++) {
      var item = aa[i]
      var rtime = roundTime(item['dt_end'])
      var rdate = item['dt_end'].split(' ')[0]
      var severity = item['severity']
      var severityIndex = parseInt(item['severity']) - 1
      var strDate = formatDate(new Date(rdate + 'T' + rtime + ':00'))
      var timeIndex = timeList.indexOf(strDate)

      if (timeIndex >= 0) {
        if (severity < 0) {
          severityIndex = 1
        } else if (severity > severityLevels) {
          severityIndex = severityLevels - 1
        }
        alertsList[severityIndex][timeIndex] += 1
      } else {
        console.warn('createAlertTimelineChart', 'wrongtimeindex', strDate, timeIndex)
      }
    }

    // Get max y value
    var maxVal = []
    for (var i = 0; i < severityLevels; i++) {
      maxVal.push(Math.max.apply(null, alertsList[i]))
    }
    var maxY = Math.max.apply(null, maxVal)

    // Create Datasets
    var datasetList = []
    for (var i = 0; i < severityLevels; i++) {
      var isNull = alertsList[i].every(function (item) { return item == 0 })
      if (isNull != true) {
        var dataItem = {
          label: 'Severity Level ' + (i + 1).toString(),
          backgroundColor: colorList[i],
          hoverBackgroundColor: colorList[i],
          borderColor: colorList[i],
          data: alertsList[i].slice(),
          fill: false,
          borderWidth: 1,
          pointRadius: 1
        }
        datasetList.push(dataItem)
      } else {
        // In case of empty alertList don't fill data
        var dataItem = {
          label: 'Severity Level ' + (i + 1).toString(),
          backgroundColor: colorList[i],
          hoverBackgroundColor: colorList[i],
          borderColor: colorList[i],
          fill: false,
          borderWidth: 1,
          pointRadius: 1
        }
        datasetList.push(dataItem)
      }
    }
  }

  // Chart config
  var ctx = document.getElementById('alerts_sum')
  var myBarChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: timeList,
      datasets: datasetList

    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      animation: {
        duration: 0
      },
      scales: {
        xAxes: [{
          stacked: true,
          time: {
            unit: 'time'
          },
          gridLines: {
            display: false,
            drawBorder: false
          },
          ticks: {
            maxTicksLimit: 6
          },
          maxBarThickness: 25,
          scaleLabel: {
            display: true,
            labelString: 'Time'
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            min: 0.0,
            max: maxY
            // maxTicksLimit: 5,
            // padding: 10,
            // Include a dollar sign in the ticks
            /* callback: function(value, index, values) {
                return numberFormat(value)+" B";
              } */

          },
          gridLines: {
            color: 'rgb(234, 236, 244)',
            zeroLineColor: 'rgb(234, 236, 244)',
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          },
          scaleLabel: {
            display: true,
            labelString: 'Total amount of alerts per severity per time'
          }
        }]
      },
      legend: {
        display: true,
        position: 'bottom'
      },
      tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: 'rgb(255,255,255)',
        bodyFontColor: '#858796',
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
          label: function (tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || ''
            return datasetLabel + '  (' + numberFormat(tooltipItem.yLabel) + ')'
          }
        }
      }
    }
  })
}

/* ---------------------- main ---------------------- */

var lastDataHash = ''

function loadData () {
  $.getJSON($SCRIPT_ROOT + '/get_alerts?lastDataHash=' + lastDataHash, {
  }, function (data) {
    if (data) {
      var status = data['status']
      var rawData = data['data']
      var dataHash = data['data_hash']

      if (data['data_hash'] != lastDataHash) {
        createAlertTimelineChart(rawData)
        createAlertMap(rawData)
        setTblCountryAlerts(rawData)
        setBoxAlerts(rawData)
        setBoxAttackers(rawData)
        setTblAlerts(rawData)
      }

      lastDataHash = dataHash
    } else {
      console.warn('Wrong data hash ')
    }
  })

  $.getJSON($SCRIPT_ROOT + '/get_port_stats', {
  }, function (data) {
    createPortChart(data)
  })

  $.getJSON($SCRIPT_ROOT + '/get_ludus_status', {
  }, function (data) {
    setBoxLudusStatus(data)
  })

  $.getJSON($SCRIPT_ROOT + '/get_uptime', {
  }, function (data) {
    setBoxUptime(data)
  })

  $.getJSON($SCRIPT_ROOT + '/get_uptime', {
  }, function (data) {
    setBoxLudusStatus(data)
  })
}

// Load after start
loadData()

// Periodicly load data
setInterval(function () { loadData() }, 3000)
