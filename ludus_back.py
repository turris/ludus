#!/usr/bin/env python3´
# -*- coding: utf-8 -*-
"""
Created on Mon May 20 17:18:30 2019

@author: cznic
"""

import maxminddb
import os
import time
from datetime import datetime
from time import mktime
import subprocess
import pickle


def java_string_hashcode(s):
    h = 0
    for c in s:
        h = (31 * h + ord(c)) & 0xFFFFFFFF
    res = ((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000
    return res % (1 << 32)


def str_to_datetime(str_date):
    t1 = time.strptime(str_date, '%Y-%m-%d %H:%M:%S.%f')
    return datetime.fromtimestamp(mktime(t1))


def call_cmd(cmd):
    assert isinstance(cmd, list)
    task = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE)
    data = task.stdout.read()
    return data


class LudusSysFunc(object):
    geoip_db_path = 'db/GeoLite2-Country.mmdb'

    @staticmethod
    def is_app_running(app_name):
        ps_list = call_cmd(["/bin/busybox", "ps"])
        for line in ps_list.splitlines():
            app = " ".join(line.split()[4:])
            if app.find(app_name) >= 0:
                return True
        return False

    @staticmethod
    def set_uci_ludus_setting(option, value):
        val = "ludus.common.%s=%s" % (option, value)
        try:
            ret = str(call_cmd(["uci", "set", val]).strip(), 'utf-8')
            call_cmd(["uci", "commit", "ludus"])
        except FileNotFoundError:
            ret = False
        return ret

    @staticmethod
    def restart_ludus_daemon():
        try:
            ret = call_cmd(["/etc/init.d/ludus", "restart"])
        except FileNotFoundError:
            ret = False
        return ret

    @staticmethod
    def get_uci_ludus_setting(option):
        val = "ludus.common.%s" % option
        try:
            ret = str(call_cmd(["uci", "get", val]).strip(), 'utf-8')
        except FileNotFoundError:
            ret = False
        return ret

    @staticmethod
    def is_process_running(pid):
        try:
            os.kill(int(pid), 0)
        except OSError:
            return False
        else:
            return True

    @staticmethod
    def get_uptime(show_seconds=False):
        try:
            f = open("/proc/uptime")
            contents = f.read().split()
            f.close()
        except:
            return "Cannot open uptime file: /proc/uptime"
        total_seconds = float(contents[0])

        # Helper vars:
        MINUTE = 60
        HOUR = MINUTE * 60
        DAY = HOUR * 24

        # Get the days, hours, etc:
        days = int(total_seconds / DAY)
        hours = int((total_seconds % DAY) / HOUR)
        minutes = int((total_seconds % HOUR) / MINUTE)
        seconds = int(total_seconds % MINUTE)

        # Build up the pretty string (like this: "N days, N hours, N minutes, N seconds")
        string = ""
        if days > 0:
            string += str(days) + (days == 1 and " day, " or " days, ")
        if (len(string) > 0 or hours > 0) and hours != 0:
            string += str(hours) + " h., "
        if (len(string) > 0 or minutes > 0) and minutes != 0:
            string += str(minutes) + " min. "
        if show_seconds is True:
            string += str(seconds) + " sec."
        return string

    @staticmethod
    def get_ip_country(ipv4):
        reader = maxminddb.open_database(LudusSysFunc.geoip_db_path)
        ret = reader.get(ipv4)
        name = None
        iso_code = None
        if ret:
            if "country" in ret:
                iso_code = ret.get("country").get("iso_code")
                name = ret.get("country").get("names").get("en")
            elif "continent" in ret:
                iso_code = ret.get("continent").get("code")
                name = ret.get("continent").get("names").get("en")
        reader.close()
        return {"name": name, "iso_code": iso_code}

    @staticmethod
    def get_foris_passwd():
        val = "foris.auth.password"
        return str(call_cmd(["uci", "get", val]).strip(), 'utf-8')


class FlowJsonData():
    def __init__(self, filename):
        self.filename = filename
        self.data = []

    def __enter__(self):
        if os.path.isfile(self.filename):
            with open(self.filename, "rb") as fp:
                self.data = pickle.load(fp)
        return self

    def __exit__(self, *args):
        pass

    def read_json(self):
        return self.data


class FlowInfo(object):
    @staticmethod
    def get_port_stats(data):
        "Get port statistics"
        stat_alert = {}
        stat_normal = {}
        for item in data:
            for flow in item["flows"]:
                alert = len(flow["alert"]) > 0
                port = flow["dport"]
                data = flow["bytes_toclient"]
                if alert is True:
                    data_all = stat_alert.get(flow["dport"], 0) + data
                    stat_alert.update({port: data_all})
                else:
                    data_all = stat_normal.get(flow["dport"], 0) + data
                    stat_normal.update({port: data_all})
        return {"alerts": stat_alert, "normal": stat_normal}

    @staticmethod
    def filter_alerts(data, max_items=None, conv_date_str=False):
        "Get sorted alerts by flow start"
        ret = []
        for item in data:
            tw_start, tw_end = item["tw_start"], item["tw_end"]
            for flow in item["flows"]:
                for alert in flow["alert"]:
                    country = LudusSysFunc.get_ip_country(flow["src_ip"])
                    ret.append({
                                "country": country["name"],
                                "country_iso": country["iso_code"],
                                "severity": alert["severity"],
                                "category": alert["category"],
                                "signature": alert["signature"],
                                "src_ip": flow["src_ip"],
                                "dport": flow["dport"],
                                "dt_start": str_to_datetime(tw_start),
                                "dt_end": str_to_datetime(tw_end)
                                })
        ret_sorted = sorted(ret, key=lambda i: i['dt_start'])
        if conv_date_str is True:
            for item in ret_sorted:
                dt_start, dt_end = item.get("dt_start"), item.get("dt_end")
                item.update({"dt_start": str(dt_start),
                             "dt_end": str(dt_end)})
        if max_items:
            return ret_sorted[:max_items]
        return ret_sorted


"""
with FlowJsonData("/home/cznic/data/tmp/ludusTEST.pkl") as fp:
    all_data = fp.read_json()
    aaa = FlowInfo.filter_alerts(all_data,conv_date_str=True)
    ccc = FlowInfo.get_port_stats(all_data)
    #json.dumps(aaa)
    print(aaa)
    print(ccc)


print(LudusSysFunc.get_uci_ludus_setting("show_welcome"))
"""


