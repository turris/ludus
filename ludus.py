#!/usr/bin/python3

from flask import request, send_from_directory, send_file
from flask import render_template
from flask import Flask
import flask
import flask_login
import os
import json
import secrets
from pbkdf2 import crypt
import ipaddress
import configparser
from configparser import NoOptionError
import datetime

def get_raw_datafile_path(config_file="/etc/ludus/ludus.config", default_raw_file="/tmp/ludus_local_data.pkl"):
    if os.path.isfile(config_file):
        config = configparser.ConfigParser()
        config.read(config_file)
        try:
            return config.get("settings", "local_stats")
        except NoOptionError:
            return default_raw_file
    else:
        return default_raw_file

if os.path.isfile("/etc/turris-version"):
    DEBUG_MODE = 0
    RAW_DATA_FILE = get_raw_datafile_path()
    from ludus_back import FlowJsonData, FlowInfo
    from ludus_back import java_string_hashcode
    from ludus_back import LudusSysFunc
else:
    DEBUG_MODE = 1
    RAW_DATA_FILE = "/home/cznic/Downloads/ludusTEST_2.pkl"
    from .ludus_back import FlowJsonData, FlowInfo
    from .ludus_back import java_string_hashcode
    from .ludus_back import LudusSysFunc

SECRET_PATH = "/tmp/web_ludus_secret.txt"
LUDUS_PID_FILE = "/var/run/ludus.pid"
LUDUS_LOG_FILE = "/tmp/log/ludus/ludus.log"
CONVERTED_JSON_FILE = "/tmp/ludus_data.json"
CONVERTED_FILE_NAME = "data-%s.json"

def get_secret(file_secret=SECRET_PATH):
    if os.path.isfile(file_secret):
        with open(file_secret, "r") as fp:
            return fp.read()
    else:
        with open(file_secret, "w") as fp:
            data = secrets.token_hex()
            fp.write(data)
            return data


login_manager = flask_login.LoginManager()
app = Flask(__name__)
app.secret_key = get_secret()
login_manager.init_app(app)



def get_ludus_log():
    if os.path.isfile(LUDUS_LOG_FILE):
        with open(LUDUS_LOG_FILE) as fp:
            return fp.read()
    return "No data"


def is_valid_ip(ip):
    try:
        ipaddress.ip_address(ip)
        return True
    except:
        return False


def is_passwd(passwd):
    if DEBUG_MODE == 1:
        foris_hash = "$p5k2$3e8$j4W4QWXF$tMEbZo2Qz4H8loFfmT/Ywa4MElBbuKxu"
    else:
        foris_hash = LudusSysFunc.get_foris_passwd()
    passwd_hash = crypt(passwd, salt=foris_hash)
    if foris_hash == passwd_hash:
        return True
    else:
        return False


class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(username):
    user = User()
    user.id = "foris"
    return user


@login_manager.request_loader
def request_loader(request):
    email = request.form.get('email')
    if email is not "foris":
        return
    user = User()
    user.id = "foris"
    user.is_authenticated = is_passwd(request.form['password'])
    return user


@app.route('/login', methods=['GET', 'POST'])
def login():
    if flask.request.method == 'GET':
        return render_template("login.html")
    if is_passwd(flask.request.form['password']):
        user = User()
        user.id = "foris"
        flask_login.login_user(user)
        return flask.redirect(flask.url_for('dashboard'))
    return flask.redirect(flask.url_for('login'))


@app.route('/logout')
def logout():
    flask_login.logout_user()
    return flask.redirect(flask.url_for('login'))


@login_manager.unauthorized_handler
def unauthorized_handler():
    return flask.redirect(flask.url_for('login'))


@app.route("/get_ludus_status")
@flask_login.login_required
def get_ludus_status():
    status = "Stopped"
    if os.path.isfile(LUDUS_PID_FILE):
        with open(LUDUS_PID_FILE, "r") as fp:
            pid = fp.read()
            if LudusSysFunc.is_process_running(pid):
                status = "Running"
    return json.dumps({"status": status})


@app.route('/dashboard', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
@flask_login.login_required
def dashboard():
    ret = True if LudusSysFunc.get_uci_ludus_setting("show_welcome") == "1" else False
    if request.method == 'POST':
        hide = request.form.get('hide_welcome')
        if hide == "1":
            LudusSysFunc.set_uci_ludus_setting("show_welcome", "0")
            ret = False
    return render_template("index.html", show_welcome=ret)


@app.route('/settings', methods=['GET', 'POST'])
@flask_login.login_required
def setting_page():
    public_ip = ""
    ret_ip = LudusSysFunc.get_uci_ludus_setting("public_ip")
    if ret_ip is not False:
        public_ip = ret_ip
    if request.method == 'POST':
        strategy = request.form.get('strategy_update')
        public_ip = request.form.get('public_ip')
        autodetect = request.form.get('autodetect_ip')
        if autodetect == "on":
            LudusSysFunc.set_uci_ludus_setting("public_ip", "")
            public_ip = ""
        else:
            if is_valid_ip(public_ip) is True:
                LudusSysFunc.set_uci_ludus_setting("public_ip", public_ip)
                LudusSysFunc.restart_ludus_daemon()
            else:
                public_ip = ""
    return render_template("settings.html", public_ip=public_ip, log_text=get_ludus_log())


@app.route('/help', methods=['GET', 'POST'])
@flask_login.login_required
def help_page():
    return render_template("help.html")


@app.route('/<name>/<path:path>')
def send_static(name, path):
    white_list = ["css", "flags", "img", "js", "scss", "vendor"]
    if name in white_list:
        return send_from_directory('static/' + name, path)


@app.route('/get_top_alerts')
@flask_login.login_required
def get_json():
    with FlowJsonData(RAW_DATA_FILE) as fp:
        return json.dumps(FlowInfo.filter_alerts(fp.read_json(),
                                                max_items=10,
                                                conv_date_str=True))


@app.route('/get_alerts', methods=['GET', 'POST'])
@flask_login.login_required
def get_all_alerts():
    with FlowJsonData(RAW_DATA_FILE) as fp:
        raw_data = FlowInfo.filter_alerts(fp.read_json(), conv_date_str=True)
        data_hash = java_string_hashcode(json.dumps(raw_data))
        data = {"status": "update", "data": raw_data, "data_hash": data_hash}
        json_data = json.dumps(data)
        recv_hash = request.args.get('lastDataHash')
        return json_data


@app.route('/get_port_stats')
@flask_login.login_required
def get_port_stats():
    with FlowJsonData(RAW_DATA_FILE) as fp:
        return json.dumps(FlowInfo.get_port_stats(fp.read_json()))


@app.route('/get_uptime')
@flask_login.login_required
def get_uptime_data():
    uptime = LudusSysFunc.get_uptime(True)
    return json.dumps({"uptime": uptime})


@app.route('/ludus/download_json', methods=['GET'])
@app.route('/download_json', methods=['GET'])
@flask_login.login_required
def download():
    if os.path.isfile(RAW_DATA_FILE):
        with FlowJsonData(RAW_DATA_FILE) as fp_raw:
            raw_data = fp_raw.read_json()
        with open(CONVERTED_JSON_FILE, "w") as fp_json:
            json.dump(raw_data, fp_json)
        datestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%m")
        filename = CONVERTED_FILE_NAME % datestamp
        return send_file(CONVERTED_JSON_FILE, as_attachment=True, attachment_filename=filename)
    else:
        return flask.redirect(flask.url_for('dashboard'))


if __name__ == "__main__":
    app.run(host='0.0.0.0')
